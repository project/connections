<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<?php print $head ?><?php print $styles ?>
</head>
<body <?php print theme("onload_attribute"); ?>> 
<div id="page"> 
  <div id="header"> 
     <div id="primary">
<?php if (count($primary_links)) : ?>
<ul id="primary">
<?php foreach ($primary_links as $link): ?>
<li><?php print $link?></li> |
<?php endforeach; ?>
</ul>
<?php endif; ?>
</div><div id="headerimg"> <div class="titleheader">
 <a href="<?php print url() ?>" title="<?php print($site_name) ?>"><?php print($site_name) ?></a> </div>
      <div class="slogan"><?php print($site_slogan) ?></div> 
    </div> 
  </div> 
  <hr /> 
  <div id="content" class="narrowcolumn"> 
    <div class="navigation"> <?php print $breadcrumb ?> </div> 
    <?php if ($messages != ""): ?> 
    <div id="message"><?php print $messages ?></div> 
    <?php endif; ?> 
    <?php if ($mission != ""): ?> 
    <div id="mission"><?php print $mission ?></div> 
    <?php endif; ?> 
    <?php if ($title != ""): ?> 
    <h2 class="page-title"><?php print $title ?></h2> 
    <?php endif; ?> 
    <?php if ($tabs != ""): ?> 
    <?php print $tabs ?> 
    <?php endif; ?> 
    <?php if ($help != ""): ?> 
    <p id="help"><?php print $help ?></p> 
    <?php endif; ?> 
    <!-- start main content --> 
    <?php print($content) ?> 
    <!-- end main content --> 
  </div> 
  <div id="sidebar"> 
    <?php if ($search_box): ?> 
    <form action="<?php print $search_url ?>" method="post" id="searchform"> 
      <input class="form-text" type="text" size="15" value="" name="keys" id="s" /> 
      <input class="form-submit" type="submit" value="<?php print $search_button_text ?>" id="searchsubmit" /> 
    </form> 
    <?php endif; ?> 
    <?php print $sidebar_left ?> <?php print $sidebar_right ?> </div> 
  <hr /> 
  <div id="footer"> 
    <p> 
      <?php if ($footer_message) : ?> 
      <?php print $footer_message;?><br /> 
      <?php endif; ?> 
      <?php print($site_name) ?> is proudly powered by <a href="http://drupal.org">Drupal</a> and almost valid <a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional (most of the time)">XHTML</a> and <a href="http://jigsaw.w3.org/css-validator/check/referer" title="Valid CSS; hopefully...">CSS</a>. <br /> 
      <a href="/node/feed/">RSS</a> <strong>|</strong> Brought to Drupal by <a href="http://journalphoenix.com">david hodgkins</a>. Design by <a href="http://www.vanillamist.com">Patricia Muller.</a> </p> 
  </div> 
</div> 
</div> 
<?php print $closure;?> 
</body>
</html>

