<div class="comment <?php print ($comment->new) ? 'comment-new' : '' ?>"> 
  <?php if ($comment->new) : ?> 
  <a id="new"></a> <span class="new"><?php print $new ?></span> 
  <?php endif; ?> 
<div class="date"><small><?php print $date ?></small> </div>
<h3 class="title"><?php print $title ?></h3>

  <?php print $picture ?> <cite><?php print $author ?> Says:</cite><br /> 
  
  <div class="content"><?php print $content ?></div> 
  <?php if ($picture) : ?> 
  <br class="clear" /> 
  <?php endif; ?> 
  <p class="postmetadata"><?php print $links ?> &#187;</p> 
</div>
